#Set the base image to Ubuntu 16.04
FROM centos:centos7

# File Author
MAINTAINER kha.vu@sentifi.com

# ====== Basic Configuration =======
# Install packages
RUN yum -y update && \
	yum -y install wget git curl telnet vim unzip

# ======= Application Configuration =======
## Google Cloud SDK
RUN yum install -y epel-release
RUN yum install -y nginx

# Start services
CMD systemctl start nginx
